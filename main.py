from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
# from starlette.middleware.cors import CORSMiddleware
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware
import starlette

from exceptions.custom_exception_handlers import custom_exception_handler, \
    custom_exception_handler_of_request_validation_error
from exceptions.custom_exceptions import CustomException

app = FastAPI(
    title='SteelGym',
    description='SteelGym --OpenAPI Spec (Swagger_UI)',
    openapi_url="/openapi.json",
    swagger_ui_parameters={"operationsSorter": "method", "tagsSorter": "alpha"},
    docs_url="/docs",
    redoc_url="/redocs",
)

# Add Middlewares to app
# It should be changed from * to all our frontend domains
ALLOWED_ORIGINS = ['*']
app.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(SessionMiddleware, secret_key='SECRET_KEY')

# Add routers to app
app_prefix = "/api"
# app.include_router(prefix=app_prefix, router=user_routes)


# Add exception handlers to app
app.add_exception_handler(CustomException, custom_exception_handler)
app.add_exception_handler(RequestValidationError, custom_exception_handler_of_request_validation_error)
