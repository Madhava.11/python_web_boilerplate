from starlette import status

from constants.enums import StatusType

INVALID_TODO_ID_EXCEPTION = (
    status.HTTP_400_BAD_REQUEST,
    StatusType.ERROR.value,
    "Invalid todo"
)

TODO_ID_NOT_FOUND_EXCEPTION = (
    status.HTTP_404_NOT_FOUND,
    StatusType.ERROR.value,
    "Todo not found"
)
