from fastapi.exceptions import RequestValidationError
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse

from constants.enums import StatusType
from exceptions.custom_exceptions import CustomException


async def custom_exception_handler(request: Request, exc: CustomException):
    """
    It will catch the custom exceptions that are raised and
    returns a Json response in given format
    status: [success, error]
    message: str
    data: {} or None
    """
    return JSONResponse(
        status_code=exc.status_code,
        content={"status": exc.custom_status, "message": exc.message, "data": exc.data},
    )


async def custom_exception_handler_of_request_validation_error(request: Request, exc: RequestValidationError):
    """
        It will catch the validation exceptions that are raised by fastapi and
        returns a Json response in given format
        status: [success, error]
        message: str
        data: {} or None
    """
    exc_str = f'{exc}'.replace('\n', ' ').replace('   ', ' ')
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"status": StatusType.ERROR.value, "message": exc_str, "data": None},
    )
